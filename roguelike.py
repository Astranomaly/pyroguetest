#############################################
### ROGUELIKE TUTORIAL ######################
#############################################

# http://www.roguebasin.com/index.php?title=Complete_Roguelike_Tutorial,_using_python%2Blibtcod,_part_10#The_main_menu
# http://www.roguebasin.com/index.php?title=Complete_Roguelike_Tutorial,_using_python%2Blibtcod,_part_10_code

#--------------------
# Import
#--------------------
import libtcodpy as libtcod
import math
import textwrap
import shelve


#--------------------
# Variables
#--------------------
SCREEN_WIDTH  = 80
SCREEN_HEIGHT = 50
LIMIT_FPS     = 20

MAP_WIDTH  = 80
MAP_HEIGHT = 43

BAR_WIDTH    = 20
PANEL_HEIGHT = 7
PANEL_Y      = SCREEN_HEIGHT - PANEL_HEIGHT

INVENTORY_WIDTH = 50

MSG_X      = BAR_WIDTH + 2
MSG_WIDTH  = SCREEN_WIDTH - BAR_WIDTH - 2
MSG_HEIGHT = PANEL_HEIGHT - 1

ROOM_MAX_SIZE     = 10
ROOM_MIN_SIZE     = 6
MAX_ROOMS         = 30
MAX_ROOM_MONSTERS = 3
MAX_ROOM_ITEMS    = 2

FOV_ALGO        = 0
FOV_LIGHT_WALLS = True
TORCH_RADIUS    = 10

HEAL_AMOUNT       = 4
LIGHTNING_RANGE   = 5
LIGHTNING_DAMAGE  = 20
CONFUSE_RANGE     = 8
CONFUSE_NUM_TURNS = 10
FIREBALL_RADIUS   = 3
FIREBALL_DAMAGE   = 12

color_dark_wall    = libtcod.Color(0,41,103)
color_light_wall   = libtcod.Color(0,102,255)
color_dark_ground  = libtcod.Color(103,61,0)
color_light_ground = libtcod.Color(255,153,0)


#--------------------
# Classes
#--------------------
class Object:
	#generic object: the player, a MOB, an item, stairs, etc...
	#always represented by onscreen character
	def __init__(self, x, y, char, name, color, blocks=False, fighter=None, ai=None, item=None):
		self.x = x
		self.y = y
		self.char = char
		self.color = color
		self.name = name
		self.blocks = blocks

		self.fighter = fighter
		self.ai = ai
		self.item = item

		#let the components know who owns them
		if self.fighter:
			self.fighter.owner = self

		if self.ai:
			self.ai.owner = self

		if self.item:
			self.item.owner = self

	def move_towards(self, target_x, target_y):
		dx = target_x - self.x
		dy = target_y - self.y
		distance = math.sqrt(dx ** 2 + dy ** 2)

		dx = int(round(dx / distance))
		dy = int(round(dy / distance))

		self.move(dx, dy)

	#return distance to coordinates
	def distance(self, x, y):
		return math.sqrt((x - self.x) ** 2 + (y - self.y) ** 2)

	#return distance to another object
	def distance_to(self, other):
		dx = other.x - self.x
		dy = other.y - self.y
		return math.sqrt(dx ** 2 + dy ** 2)

	def move(self, dx, dy):
		#move by given amount, if the destination is not blocked
		if not is_blocked(self.x + dx, self.y + dy):
			self.x += dx
			self.y += dy

	def draw(self):
		#only show if visible to the player
		if libtcod.map_is_in_fov(fov_map, self.x, self.y):
			#set the color and then draw the character at its position
			libtcod.console_set_default_foreground(con, self.color)
			libtcod.console_put_char(con, self.x, self.y, self.char, libtcod.BKGND_NONE)

	#make this object draw first so others appear above it on the same tile
	def send_to_back(self):
		global objects
		objects.remove(self)
		objects.insert(0, self)

	def clear(self):
		#erase the character
		libtcod.console_put_char(con, self.x, self.y, ' ', libtcod.BKGND_NONE)

#combat related properties and mothods
class Fighter:
	def __init__(self, hp, defense, power, death_function=None):
		self.max_hp  = hp
		self.hp      = hp
		self.defense = defense
		self.power   = power
		self.death_function = death_function

	#apply damage if possible
	def take_damage(self, damage):
		if damage > 0:
			self.hp -= damage

			#check for death
			if self.hp <= 0:
				self.hp = 0
				function = self.death_function
				if function is not None:
					function(self.owner)

	#a simple attack formula
	def attack(self, target):
		damage = self.power - target.fighter.defense

		if damage > 0:
			message(self.owner.name.capitalize()+' attacks '+target.name+' for '+str(damage)+' HP.')
			target.fighter.take_damage(damage)
		else:
			message(self.owner.name.capitalize()+' attacks '+target.name+' but it has no effect!')

	#heal by given amount without going over the maximum
	def heal(self, amount):
		self.hp += amount
		if self.hp > self.max_hp:
			self.hp = self.max_hp

#AI for a basic monster
class BasicMonster:
	def take_turn(self):
		monster = self.owner
		if libtcod.map_is_in_fov(fov_map, monster.x, monster.y):

			#move towards player if too far to attack
			if monster.distance_to(player) >= 2:
				monster.move_towards(player.x, player.y)

			#attack if close enough
			elif player.fighter.hp > 0:
				monster.fighter.attack(player)

#AI for a confused monster
class ConfusedMonster:
	def __init__(self, old_ai, num_turns=CONFUSE_NUM_TURNS):
		self.old_ai = old_ai
		self.num_turns = num_turns

	def take_turn(self):
		#while confused...
		if self.num_turns > 0:
			#move in a random direction
			self.owner.move(libtcod.random_get_int(0, -1, 1), libtcod.random_get_int(0, -1, 1))
			self.num_turns -= 1
		#otherwise restore previous AI
		else:
			self.owner.ai = self.old_ai
			message('The '+self.owner.name+' is no longer confused!', libtcod.red)

#an item that can be picked up and used
class Item:
	def __init__(self, use_function=None):
		self.use_function = use_function

	def pick_up(self):
		#add to inventory and remove from map
		if len(inventory) >= 26:
			message('Your inventory is too full, you cannot cram that '+self.owner.name+' in there', libtcod.red)
		else:
			inventory.append(self.owner)
			objects.remove(self.owner)
			message('You picked up a '+self.owner.name+'!', libtcod.green)

	def drop(self):
		#add to the map at player's coords, remove from inventory
		objects.append(self.owner)
		inventory.remove(self.owner)
		self.owner.x = player.x
		self.owner.y = player.y
		message('You dropped a '+self.owner.name+'.',libtcod.yellow)

	def use(self):
		#only call the "use_function" if it is defined
		if self.use_function is None:
			message('The ' + self.owner.name + ' cannot be used.')
		#otherwise destroy after one use unless cancelled
		else:
			if self.use_function() != 'cancelled':
				inventory.remove(self.owner)

class Tile:
	#a tile of the map
	def __init__(self, blocked, block_sight = None):
		self.blocked  = blocked
		self.explored = False

		#by default, if a tile is blocked, it also blocks sight
		if block_sight is None: block_sight = blocked
		self.block_sight = block_sight

class Rect:
	#a rectangle on the map. used to characterize a room
	def __init__(self, x, y, w, h):
		self.x1 = x
		self.y1 = y
		self.x2 = x + w
		self.y2 = y + h

	def center(self):
		center_x = (self.x1 + self.x2) / 2
		center_y = (self.y1 + self.y2) / 2
		return (center_x, center_y)

	def intersect(self, other):
		#return true if this rectangle intersects another
		return (self.x1 <= other.x2 and self.x2 >= other.x1 and
		        self.y1 <= other.y2 and self.y2 >= other.y1)


#--------------------
# Functions
#--------------------
def new_game():
	global player, inventory, game_msgs, game_state

	#create object representing the player
	fighter_component = Fighter(hp=60, defense=2, power=5, death_function=player_death)
	player = Object(0, 0, '@', 'player', libtcod.white, blocks=True, fighter=fighter_component)

	#generate map, but not on screen
	make_map()
	initialize_fov()

	game_state = 'playing'
	inventory = []
	game_msgs = []

	message('Welcome stranger! Prepare to perish in the Tombs of the Ancient Kings :>', libtcod.red)

def initialize_fov():
	global fov_recompute, fov_map

	fov_recompute = True
	#create the FOV map according to the generated map
	fov_map = libtcod.map_new(MAP_WIDTH, MAP_HEIGHT)
	for y in range(MAP_HEIGHT):
		for x in range(MAP_WIDTH):
			libtcod.map_set_properties(fov_map, x, y, not map[x][y].block_sight, not map[x][y].blocked)

	#unexplored areas start black (which is the default background color)
	libtcod.console_clear(con)

def main_menu():
	img = libtcod.image_load('menu_back.png')

	while not libtcod.console_is_window_closed():
		#show the background image, at twice regular console res
		libtcod.image_blit_2x(img, 0, 0, 0)

		#show game title and stuff
		libtcod.console_set_default_foreground(0, libtcod.light_yellow)
		libtcod.console_print_ex(0, SCREEN_WIDTH/2, SCREEN_HEIGHT/2-4, libtcod.BKGND_NONE, libtcod.CENTER, 'ORIGINAL ROGUELIKE DO NOT STEAL')
		libtcod.console_print_ex(0, SCREEN_WIDTH/2, SCREEN_HEIGHT-2, libtcod.BKGND_NONE, libtcod.CENTER, 'by Astranomaly')

		#show options and wait for player choice
		choice = menu('', ['Play a new game', 'Continue last game', 'Quit'], 24)

		if choice == 0:
			#new game
			new_game()
			play_game()
		if choice == 1:
			#load last game
			try:
				load_game()
			except:
				msgbox('\n No saved game to load.\n', 24)
				continue
			play_game()
		elif choice == 2:
			#quit
			break

def play_game():
	global key, mouse

	player_action = None

	mouse = libtcod.Mouse()
	key = libtcod.Key()

	while not libtcod.console_is_window_closed():
		#render the screen
		libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS|libtcod.EVENT_MOUSE, key, mouse)
		render_all()

		libtcod.console_flush()

		#erase all objects at their old locations, before they move
		for object in objects:
			object.clear()

		#handle keys and exit game if needed
		player_action = handle_keys()
		if player_action == 'exit':
			save_game()
			break

		if game_state == 'playing' and player_action != 'didnt-take-turn':
			for object in objects:
				if object.ai:
					object.ai.take_turn()

def is_blocked(x, y):
	#test the map tile
	if map[x][y].blocked:
		return True

	#check for any blocking objects
	for object in objects:
		if object.blocks and object.x == x and object.y == y:
			return True

	return False

def create_room(room):
	global map
	#go through the tiles in the rectangle and make them passable
	for x in range(room.x1 + 1, room.x2):
		for y in range(room.y1 + 1, room.y2):
			map[x][y].blocked     = False
			map[x][y].block_sight = False

def create_h_tunnel(x1, x2, y):
	global map
	for x in range(min(x1, x2), max(x1, x2) + 1):
		map[x][y].blocked     = False
		map[x][y].block_sight = False

def create_v_tunnel(y1, y2, x):
	global map
	for y in range(min(y1, y2), max(y1, y2) + 1):
		map[x][y].blocked     = False
		map[x][y].block_sight = False

def place_object(room):
	#choose random number of monsters
	num_monsters = libtcod.random_get_int(0, 0, MAX_ROOM_MONSTERS)

	for i in range(num_monsters):
		#choose random spot for the monster
		y = libtcod.random_get_int(0, room.y1+1, room.y2-1)
		x = libtcod.random_get_int(0, room.x1+1, room.x2-1)

		#only place if the tile is not blocked
		if not is_blocked(x, y):

			#chances: 20% troll, 40% orc, 10% snake, 30% Dan:
			choice = libtcod.random_get_int(0, 0, 100)
			if choice < 20:
				#create troll
				fighter_component = Fighter(hp=16, defense=1, power=4, death_function=monster_death)
				ai_component = BasicMonster()
				monster = Object(x, y, 'T', 'troll', libtcod.darker_green, blocks=True, fighter=fighter_component, ai=ai_component)
			elif choice < 20+40:
				#create orc
				fighter_component = Fighter(hp=10, defense=0, power=3, death_function=monster_death)
				ai_component = BasicMonster()
				monster = Object(x, y, 'o', 'orc', libtcod.desaturated_green, blocks=True, fighter=fighter_component, ai=ai_component)
			elif choice < 20+40+10:
				#create snake
				fighter_component = Fighter(hp=8, defense=0, power=5, death_function=monster_death)
				ai_component = BasicMonster()
				monster = Object(x, y, 'S', 'snake', libtcod.green, blocks=True, fighter=fighter_component, ai=ai_component)
			else:
				#create Dan
				fighter_component = Fighter(hp=12, defense=2, power=6, death_function=monster_death)
				ai_component = BasicMonster()
				monster = Object(x, y, 'D', 'Dan', libtcod.desaturated_red, blocks=True, fighter=fighter_component, ai=ai_component)

			objects.append(monster)

	#choose a random number of items
	num_items = libtcod.random_get_int(0, 0, MAX_ROOM_ITEMS)

	for i in range(num_items):
		#choose a random spot for the items
		x = libtcod.random_get_int(0, room.x1+1, room.x2-1)
		y = libtcod.random_get_int(0, room.y1+1, room.y2-1)

		#only place if the tile is not blocked
		if not is_blocked(x, y):
			dice = libtcod.random_get_int(0, 0, 100)

			#create a healing potion (70%)
			if dice < 70:
				item_component = Item(use_function=cast_heal)
				item = Object(x, y, '!', 'healing potion', libtcod.violet, item=item_component)
			#create a lightning scroll (10%)
			elif dice < 70+10:
				item_component = Item(use_function=cast_lightning)
				item = Object(x, y, '#', 'scroll of lightning bolt', libtcod.light_yellow, item=item_component)
			#create a fireball scroll (10%)
			elif dice < 70+10+10:
				item_component = Item(use_function=cast_fireball)
				item = Object(x, y, '#', 'scroll of fireball', libtcod.light_yellow, item=item_component)
			#create scroll of confusion (10%)
			else:
				item_component = Item(use_function=cast_confuse)
				item = Object(x, y, '#', 'scroll of confusion', libtcod.light_yellow, item=item_component)

			objects.append(item)
			item.send_to_back()

def make_map():
	global map, objects

	objects = [player]

	#fill map with "unblocked" tiles
	map = [[ Tile(True)
		for y in range(MAP_HEIGHT)]
			for x in range(MAP_WIDTH)]

	rooms = []
	num_rooms = 0

	for r in range(MAX_ROOMS):
		#random width and height
		w = libtcod.random_get_int(0, ROOM_MIN_SIZE, ROOM_MAX_SIZE)
		h = libtcod.random_get_int(0, ROOM_MIN_SIZE, ROOM_MAX_SIZE)
		#random position without going out-of-bounds
		x = libtcod.random_get_int(0, 0, MAP_WIDTH - w - 1)
		y = libtcod.random_get_int(0, 0, MAP_HEIGHT - h - 1)

		new_room = Rect(x, y, w, h)

		#run through other rooms and see if they intersect with this one
		failed = False
		for other_room in rooms:
			if new_room.intersect(other_room):
				failed = True
				break

		if not failed:
			#no intersection, so this room is valid, "paint" it to the map
			create_room(new_room)

			#add contents to room
			place_object(new_room)

			#center coordinates of new room
			(new_x, new_y) = new_room.center()

			### OPTIONAL: print "room letter" to see how the map drawing worked
			#room_no = Object(new_x, new_y, chr(65+num_rooms), 'room number' libtcod.white)
			#objects.insert(0, room_no) #draw early, so monsters are drawn on top

			#place the player in the first room
			if num_rooms == 0:
				player.x = new_x
				player.y = new_y
			else:
				#connect all other rooms via tunnels

				#center coords of previous room
				(prev_x, prev_y) = rooms[num_rooms - 1].center()
				#50/50 chance
				if libtcod.random_get_int(0, 0, 1) == 1:
					#first move hor, then vert
					create_h_tunnel(prev_x, new_x, prev_y)
					create_v_tunnel(prev_y, new_y, new_x)
				else:
					#first move vert, then hor
					create_v_tunnel(prev_y, new_y, prev_x)
					create_h_tunnel(prev_x, new_x, new_y)

			#append new room to list
			rooms.append(new_room)
			num_rooms += 1

def render_all():
	global fov_map, color_dark_wall, color_light_wall
	global color_dark_ground, color_light_ground
	global fov_recompute

	if fov_recompute:
		#recompute FOV if the player moved or something
		fov_recompute = False
		libtcod.map_compute_fov(fov_map, player.x, player.y, TORCH_RADIUS, FOV_LIGHT_WALLS, FOV_ALGO)

		#go through all tiles, set their background color according to FOV
		for y in range(MAP_HEIGHT):
			for x in range(MAP_WIDTH):
				visible = libtcod.map_is_in_fov(fov_map, x, y)
				wall = map[x][y].block_sight
				if not visible:
					#if not visible right now, the player can only see it if it's explored
					if map[x][y].explored:
						#it's out of player's FOV
						if wall:
							libtcod.console_set_char_background(con, x, y, color_dark_wall, libtcod.BKGND_SET)
						else:
							libtcod.console_set_char_background(con, x, y, color_dark_ground, libtcod.BKGND_SET)
				else:
					#it's visible
					if wall:
						libtcod.console_set_char_background(con, x, y, color_light_wall, libtcod.BKGND_SET)
					else:
						libtcod.console_set_char_background(con, x, y, color_light_ground, libtcod.BKGND_SET)
					#since it's visible, explore it
					map[x][y].explored = True

	#draw all objects in the list
	for object in objects:
		if object != player:
			object.draw()
	player.draw()

	#blit the contents of "con" to the root console
	libtcod.console_blit(con, 0, 0, MAP_WIDTH, MAP_HEIGHT, 0, 0, 0)

	#prepare to render the GUI panel
	libtcod.console_set_default_background(panel, libtcod.black)
	libtcod.console_clear(panel)

	#print the game msgs one line at a time
	y = 1
	for (line, color) in game_msgs:
		libtcod.console_set_default_foreground(panel, color)
		libtcod.console_print_ex(panel, MSG_X, y, libtcod.BKGND_NONE, libtcod.LEFT, line)
		y += 1

	#show the player's stats
	render_bar(1, 1, BAR_WIDTH, 'HP', player.fighter.hp, player.fighter.max_hp,
		libtcod.light_red, libtcod.darker_red)

	#display the names of objects under mouse
	libtcod.console_set_default_foreground(panel, libtcod.light_grey)
	libtcod.console_print_ex(panel, 1, 0, libtcod.BKGND_NONE, libtcod.LEFT, get_names_under_mouse())

	#blit the contents of "panel" to the root console
	libtcod.console_blit(panel, 0, 0, MAP_WIDTH, PANEL_HEIGHT, 0, 0, PANEL_Y)

# Key handler
def handle_keys():
	global key;

	if key.vk == libtcod.KEY_ENTER and key.lalt:
		#ALT+Enter: toggle fullscreen
		libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen())
	elif key.vk == libtcod.KEY_ESCAPE:
		# Exit game
		return 'exit'

	if game_state == 'playing':
		# movement
		if key.vk == libtcod.KEY_UP:
			player_move_or_attack(0, -1)

		elif key.vk == libtcod.KEY_DOWN:
			player_move_or_attack(0, 1)

		elif key.vk == libtcod.KEY_LEFT:
			player_move_or_attack(-1, 0)

		elif key.vk == libtcod.KEY_RIGHT:
			player_move_or_attack(1, 0)

		else:
			#test for other keys
			key_char = chr(key.c)

			if key_char == 'g':
				#pick up item
				for object in objects:
					if object.x == player.x and object.y == player.y and object.item:
						object.item.pick_up()
						break
			if key_char == 'i':
				#show the inventory, and if an item is selected, use it
				chosen_item = inventory_menu('Press the key next to an item to use it, or any other key to cancel.\n')
				if chosen_item is not None:
					chosen_item.use()
			if key_char == 'd':
				#show the inventory; if an item is selected, drop it
				chosen_item = inventory_menu('Press the key next to an item to drop it, or any other key to cancel.\n')
				if chosen_item is not None:
					chosen_item.drop()

			return 'didnt-take-turn'

def target_tile(max_range=None):
	global key, mouse

	#return the position of a tile L-clicked in a player's FOV (in range), cancelled on Rclick
	while True:
		#render the screen. replaces inventory and shows names of object under mouse
		libtcod.console_flush()
		libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS|libtcod.EVENT_MOUSE,key,mouse)
		render_all()

		(x, y) = (mouse.cx, mouse.cy)

		if mouse.rbutton_pressed or key.vk == libtcod.KEY_ESCAPE:
			# cancel on R-click and Esc
			return (None, None)

		#target only if within FOV and range (if specified)
		if (mouse.lbutton_pressed and libtcod.map_is_in_fov(fov_map, x, y) and (max_range is None or player.distance(x, y) <= max_range)):
			return(x, y)

def target_monster(max_range=None):
	#return the monster L-clicked in a player's FOV (in range), cancelled on Rclick
	while True:
		(x, y) = target_tile(max_range)
		#cancelled
		if x is None:
			return None
		#return the first clicked monster, otherwise loop
		for obj in objects:
			if obj.x == x and obj.y == y and obj.fighter and obj != player:
				return obj

def get_names_under_mouse():
	global mouse

	#return a string with the names of all objects under mouse
	(x, y) = (mouse.cx, mouse.cy)

	#create a list with the names of all objects at the mouse's coordinates and in FOV
	names = [obj.name for obj in objects
		if obj.x == x and obj.y == y and libtcod.map_is_in_fov(fov_map, obj.x, obj.y)]
	#return the names seperated by commas
	names = ', '.join(names)
	return names.capitalize()

def player_move_or_attack(dx, dy):
	global fov_recompute

	#the coords the player is moving to/attacking
	x = player.x + dx
	y = player.y + dy

	#find attackable objects
	target = None
	for object in objects:
		if object.fighter and object.x == x and object.y == y:
			target = object
			break

	#attack if target is found, otherwise move
	if target is not None:
		player.fighter.attack(target)
	else:
		player.move(dx, dy)
		fov_recompute = True

def cast_heal():
	#heal the player
	if player.fighter.hp == player.fighter.max_hp:
		message('You are already at full health.', libtcod.red)
		return 'cancelled'

	message('Your wounds start to feel better!', libtcod.light_violet)
	player.fighter.heal(HEAL_AMOUNT)

def cast_lightning():
	monster = closest_monster(LIGHTNING_RANGE)
	#if there is no monster nearby
	if monster is None:
		message('No enemy is close enough to strike.', libtcod.red)
		return 'cancelled'
	#zap it
	message('A lightning bolt strikes the '+monster.name+' with a loud thunder! The damage is '+str(LIGHTNING_DAMAGE)+'HP.', libtcod.light_blue)
	monster.fighter.take_damage(LIGHTNING_DAMAGE)

def cast_confuse():
	#ask for target
	message('Left-click an enemy to confuse it, or right-click to cancel.', libtcod.light_cyan)
	monster = target_monster(CONFUSE_RANGE)
	if monster is None: return 'cancelled'
	#replace AI with "confused AI"
	old_ai = monster.ai
	monster.ai = ConfusedMonster(old_ai)
	monster.ai.owner = monster
	message('The eyes of the '+monster.name+' look vacant as it starts to stumble around', libtcod.light_green)

def cast_fireball():
	#ask for target
	message('Left-click a target tile for the fireball, or right-click to cancel.', libtcod.light_cyan)
	(x, y) = target_tile()
	if x is None: return 'cancelled'
	message('The fireball explodes, burning everything within '+str(FIREBALL_RADIUS)+' tiles!', libtcod.orange)

	#damage every fighter in range, including player
	for obj in objects:
		if obj.distance(x, y) <= FIREBALL_RADIUS and obj.fighter:
			message('The '+obj.name+' gets burned for '+str(FIREBALL_DAMAGE)+'HP.', libtcod.orange)
			obj.fighter.take_damage(FIREBALL_DAMAGE)

#find closest enemy within range and in the FOV
def closest_monster(max_range):
	closest_enemy = None
	closest_dist = max_range + 1

	for object in objects:
		if object.fighter and not object == player and libtcod.map_is_in_fov(fov_map, object.x, object.y):
			#calculate distance between this object and the player
			dist = player.distance_to(object)
			if dist < closest_dist:
				closest_enemy = object
				closest_dist = dist
	return closest_enemy

def monster_death(monster):
	message(monster.name.capitalize()+' was killed!',libtcod.orange)
	monster.char    = '%'
	monster.color   = libtcod.dark_red
	monster.blocks  = False
	monster.fighter = None
	monster.ai      = None
	monster.name    = 'Corpse of '+monster.name
	monster.send_to_back()

#game over
def player_death(player):
	global game_state

	message('You died!',libtcod.red)
	game_state = 'dead'

	player.char  = '%'
	player.color = libtcod.dark_red

def render_bar(x, y, total_width, name, value, maximum, bar_color, back_color):
	#render a bar (HP, experience, etc). first calculate the width of the bar
	bar_width = int(float(value) / maximum * total_width)

	#render the background first
	libtcod.console_set_default_background(panel, back_color)
	libtcod.console_rect(panel, x, y, total_width, 1, False, libtcod.BKGND_SCREEN)

	#now render the bar on top
	libtcod.console_set_default_background(panel, bar_color)
	if bar_width > 0:
		libtcod.console_rect(panel, x, y, bar_width, 1, False, libtcod.BKGND_SCREEN)
	#finally, some centered text with the values
	libtcod.console_set_default_foreground(panel, libtcod.white)
	libtcod.console_print_ex(panel, x + total_width / 2, y, libtcod.BKGND_NONE, libtcod.CENTER,
		name + ': ' + str(value) + '/' + str(maximum))

def message(new_msg, color = libtcod.white):
	new_msg_lines = textwrap.wrap(new_msg, MSG_WIDTH)

	for line in new_msg_lines:
		#if buffer is full remove the first line to make room for the new one
		if len(game_msgs) == MSG_HEIGHT:
			del game_msgs[0]

		#add the new line as a tuple with the text and color
		game_msgs.append((line, color))

#used for displaying menus
def menu(header, options, width):
	if len(options) > 26: raise ValueError('Cannot have a menu with more than 26 options.')

	#calculate total height for the header (after auto-wrap) and one line per option
	header_height = libtcod.console_get_height_rect(con, 0, 0, width, SCREEN_HEIGHT, header)
	if header == '':
		header_height = 0
	height = len(options) + header_height

	#create an off-screen console that represents the menu's window
	window = libtcod.console_new(width, height)

	#print the header, with auto-wrap
	libtcod.console_set_default_foreground(window, libtcod.white)
	libtcod.console_print_rect_ex(window, 0, 0, width, height, libtcod.BKGND_NONE, libtcod.LEFT, header)

	#print all the options
	y = header_height
	letter_index = ord('a')
	for option_text in options:
		text = '(' + chr(letter_index) + ') ' + option_text
		libtcod.console_print_ex(window, 0, y, libtcod.BKGND_NONE, libtcod.LEFT, text)
		y += 1
		letter_index += 1

	#blit the contents of "window" to the root console
	x = SCREEN_WIDTH/2 - width/2
	y = SCREEN_HEIGHT/2 - height/2
	libtcod.console_blit(window, 0, 0, width, height, 0, x, y, 1.0, 0.7)

	#present the root console to the player and wait for a key-press
	libtcod.console_flush()
	key = libtcod.console_wait_for_keypress(True)

	#special case, alt+enter toggle fullscreen
	if key.vk == libtcod.KEY_ENTER and key.lalt:
		libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen())

	#convert the ASCII code to an index; if it corresponds to an option, return it
	index = key.c - ord('a')
	if index >= 0 and index < len(options): return index
	return None

#show a menu with each item of the inventory as an option
def inventory_menu(header):
	if len(inventory) == 0:
		options = ['Inventory is empty']
	else:
		options = [item.name for item in inventory]

	index = menu(header, options, INVENTORY_WIDTH)

	#if an item was chosen, return it
	if index is None or len(inventory) == 0: return None
	return inventory[index].item

def msgbox(text, width=50):
	#use menu() as a message box
	menu(text, [], width)

def save_game():
	#open a new empty shelve (possibly overwriting an old one) to write the game data
	file = shelve.open('savegame', 'n')
	file['map'] = map
	file['objects'] = objects
	file['player_index'] = objects.index(player)
	file['inventory'] = inventory
	# file['game_msgs'] = game_msgs
	file['game_state'] = game_state
	file.close()

def load_game():
	#open the previously saved shelve and load the game data
	global map, objects, player, inventory, game_msgs, game_state

	file = shelve.open('savegame', 'r')
	map = file['map']
	objects = file['objects']
	player = objects[file['player_index']]  #get index of player in objects list and access it
	inventory = file['inventory']
	game_msgs = file['game_msgs']
	game_state = file['game_state']
	file.close()

	initialize_fov()


#--------------------
# Init & Main Loop
#--------------------
libtcod.console_set_custom_font('prestige12x12_gs_tc.png', libtcod.FONT_TYPE_GREYSCALE | libtcod.FONT_LAYOUT_TCOD)
libtcod.console_init_root(SCREEN_WIDTH, SCREEN_HEIGHT, 'python/ORIGINAL ROGUELIKE DO NOT STEAL', False)
libtcod.sys_set_fps(LIMIT_FPS)

con = libtcod.console_new(MAP_WIDTH, MAP_HEIGHT)
panel = libtcod.console_new(SCREEN_WIDTH, PANEL_HEIGHT)

main_menu()