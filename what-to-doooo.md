
# DISPLAY
## graphics
	ascii vs block vs graphic
### ui
## platform

# CONTROLS

# AUDIO

# MECHANICS
## progression
## generation
### rooms
### tiles
### traps
### secrets
### items
### scenery
## fow/fov
## turns
## buffs
## debuffs
## classes
## combat
### weapon degradation
### armor
### "rings" (buff items)
### "magic"

# PLAYER
## creation
## customization
## inventory

# ENEMIES

# ITEMS
## weapons
### ranged
### melee
### "magic"
## ammo
## armor
## "potions"
## "scrolls"

# THE STORY